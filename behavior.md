# Behavior


## Operating as a software engineer


### Work on things that matter


#### Glue work
Tanya Reilly has a good blog on [glue work](https://noidea.dog/glue).

### Manage technical quality


### Stay aligned with the team and leaders


### To be a leader, you have to follow


### Create space for others


### Build a network of peers


## Pull requests


### Opening a pull request


### Reviewing a pull request
1. Be thorough in the review to reduce iterations and time wasted through communication, waiting, and context switching.
2. Communicate how strongly you feel about feedback. Typically, we prefix comments with. For example, prefix comments with "nitpick" or just "nit" for something nitpick-y that you want changed or improved. Usually something small.
3. Identify straightforward ways to simplify the code while respecting scope.
4. Again, a PR should be narrowly focused. It should, within reason, do one change well. If author needs to reduce scope of change, consider saying so.
5. Offer alternative solutions, but assume that the author already considered them. For example, "What do you think about using a custom validator here?"
6. Checkout the branch and try the changes locally. You can determine how much manual testing to preform.
7. If you don't understand the code, you're probably not the only one. "Can you explain the dynamic programming used here?"
8. 